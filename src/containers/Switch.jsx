import React from "react";
import { Switch as RouterSwitch, Route } from "react-router-dom";
import ListTender from "../pages/ListTender";
import CreateTender from "../pages/CreateTender";
import EditTender from "../pages/EditTender";

export default function Switch() {
  return (
    <RouterSwitch>
      <Route exact path="/">
        <ListTender />
      </Route>
      <Route path="/create">
        <CreateTender />
      </Route>
      <Route path="/edit/:id" component={EditTender} />
    </RouterSwitch>
  );
}

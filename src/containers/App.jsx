import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter as Router } from "react-router-dom";
import { positions, Provider } from "react-alert";
import AlertTemplate from "../components/AlertTemplate";
import "./styles.css";

import Navbar from "../components/Navbar";
import Content from "../components/Content";
import Switch from "./Switch";
import Footer from "../components/Footer";

export default function App() {
  const alertOptions = {
    timeout: 5000,
    position: positions.BOTTOM_RIGHT,
  };

  return (
    <Router>
      <Provider template={AlertTemplate} {...alertOptions}>
        <div>
          <Navbar />
          <Content>
            <Switch />
          </Content>
          <Footer />
        </div>
      </Provider>
    </Router>
  );
}

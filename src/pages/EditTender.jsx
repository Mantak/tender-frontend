import React, { useState, useEffect } from "react";
import { Container } from "react-bootstrap";
import { useParams } from "react-router-dom";
import Panel from "../components/Panel";
import Form from "../components/Form";
import { useAlert } from "react-alert";
import axios from "axios";
import config from "../config";

export default function EditTender() {
  const { id } = useParams();

  const [inProcess, setInProcess] = useState(false);
  const [tender, setTender] = useState(null);
  const alert = useAlert();

  useEffect(() => {
    const fetchTender = async () => {
      const result = await axios(`${config.apiUrl}/tenders/${id}`);
      const data = result.data.data;
      setTender({ title: data.title, description: data.description });
    };
    fetchTender();
  }, [id]);

  function handleEdit(tender) {
    setInProcess(true);
    axios.patch(config.apiUrl + `/tenders/${id}`, tender).then(res => {
      alert.success("You have successfully edited tender!");
      setInProcess(false);
    }).catch(error => {
      const message = error.response.data.message;
      alert.error(message);
      setInProcess(false);
    });
  }

  return (
    <Container>
      <Panel className="mt-5">
        {tender ? (
          <div>
            <h1>Edit "{tender.title}"</h1>
            <Form onEdit={handleEdit} tender={tender} isDisabled={inProcess} edit />
          </div>
        ) : (
          <p>Loading...</p>
        )}
      </Panel>
    </Container>
  );
}

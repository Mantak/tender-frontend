import React, { useState } from "react";
import { Container } from "react-bootstrap";
import Panel from "../components/Panel";
import Form from "../components/Form";
import { useAlert } from "react-alert";
import axios from "axios";
import config from "../config";

export default function CreateTender() {
  const [inProcess, setInProcess] = useState(false);
  const alert = useAlert();

  function handleCreate(tender) {
    setInProcess(true);
    axios
      .post(config.apiUrl + "/tenders", tender)
      .then(res => {
        alert.success("You have successfully created tender!");
        setInProcess(false);
      })
      .catch(error => {
        const message = error.response.data.message;
        alert.error(message);
        setInProcess(false);
      });
  }

  return (
    <Container>
      <Panel className="mt-5">
        <h1>Create Tender</h1>
        <Form onCreate={handleCreate} isDisabled={inProcess} />
      </Panel>
    </Container>
  );
}

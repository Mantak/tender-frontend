import React, { useState, useEffect } from "react";
import { Container, Row, Col, Modal, Button } from "react-bootstrap";
import ReactPaginate from "react-paginate";
import Panel from "../components/Panel";
import Card from "../components/Card";
import { useAlert } from "react-alert";
import axios from "axios";
import config from "../config";
import { withRouter, useLocation, Link } from "react-router-dom";

function ListTender(props) {
  const [tenders, setTenders] = useState(null);
  const [showDelete, setShowDelete] = useState(false);
  const [currentPage, setCurrentPage] = useState(null);
  const [deleteId, setDeleteId] = useState(null);
  const [meta, setMeta] = useState([]);
  const alert = useAlert();
  const query = useQuery();

  useEffect(() => {
    const page = currentPage || query.get("page");
    fetchTenders(page);
  }, [currentPage]);

  const fetchTenders = async page => {
    const result = await axios(`${config.apiUrl}/tenders?page=${page || 1}`);
    setTenders(result.data.data);
    setMeta(result.data.meta);
  };

  function useQuery() {
    return new URLSearchParams(useLocation().search);
  }

  function handleDelete(id) {
    setDeleteId(id);
    setShowDelete(true);
  }

  function handleDeleteClose(confirm = false) {
    setShowDelete(false);
    confirm && deleteTender(deleteId);
  }

  function deleteTender(id) {
    axios
      .delete(`${config.apiUrl}/tenders/${id}`)
      .then(res => {
        setTenders(prevItems =>
          prevItems.filter(tender => tender.data.tender_id !== id)
        );
        fetchTenders(currentPage);
        alert.success("You have successfully deleted tender!");
      })
      .catch(error => alert.error("Error occured during tender deletion."));
  }

  function handlePageClick(clickedPage) {
    props.history.push(`/?page=${clickedPage.selected + 1}`);
    setCurrentPage(clickedPage.selected + 1);
  }

  return (
    <Container>
      <Panel className="mt-5">
        <h1>List Tender</h1>
        {tenders && tenders.length > 0 ? (
          <div className="mt-4">
            <p>Current Page: {meta.current_page}</p>
            <Row className="mt-4">
              {tenders.map(tender => (
                <Col
                  key={tender.data.tender_id}
                  lg="4"
                  md="6"
                  sm="12"
                  className="pb-4"
                >
                  <Card
                    id={tender.data.tender_id}
                    title={tender.data.title}
                    description={tender.data.description}
                    onDelete={handleDelete}
                  />
                </Col>
              ))}
            </Row>
            {meta.current_page && <ReactPaginate
              breakClassName={"page-link"}
              pageCount={meta.last_page}
              initialPage={meta.current_page-1}
              onPageChange={handlePageClick}
              containerClassName={"pagination"}
              pageLinkClassName={"page-link"}
              activeClassName={"active"}
              pageClassName={"page-item"}
              nextClassName={"page-item"}
              previousClassName={"page-item"}
              previousLinkClassName={"page-link"}
              nextLinkClassName={"page-link"}
              disableInitialCallback
            />}
          </div>
        ) : tenders ? (
          meta.current_page > meta.last_page ? (
            <p className="mt-4">This page was not found :(</p>
          ) : (
            <p className="mt-4">
              No Tenders on list. Please <Link to="/create">create one</Link> :)
            </p>
          )
        ) : (
          <p className="mt-4">Loading...</p>
        )}
      </Panel>
      <Modal show={showDelete} onHide={() => handleDeleteClose(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Delete Confirmation</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Do you really want to delete this tender? <i>There is not revert.</i>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="light" onClick={() => handleDeleteClose(false)}>
            Cancel
          </Button>
          <Button variant="danger" onClick={() => handleDeleteClose(true)}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
}

export default withRouter(ListTender);

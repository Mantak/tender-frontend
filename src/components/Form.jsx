import React, { useState } from "react";
import { Form as BForm, Button } from "react-bootstrap";

export default function Form(props) {
  const [tender, setTender] = useState({
    title: "",
    description: "",
    ...props.tender
  });

  function handleChange(event) {
    const { name, value } = event.target;
    setTender(prevValues => {
      return {
        ...prevValues,
        [name]: value
      };
    });
  }

  return (
    <BForm
      onSubmit={event => {
        event.preventDefault();

        props.edit ? props.onEdit(tender) : props.onCreate(tender);
      }}
      className="mt-4"
    >
      <BForm.Group>
        <BForm.Label>Title</BForm.Label>
        <BForm.Control
          type="text"
          name="title"
          onChange={handleChange}
          placeholder="My Tender"
          value={tender.title}
          disabled={props.isDisabled}
          required
        />
      </BForm.Group>
      <BForm.Group>
        <BForm.Label>Description</BForm.Label>
        <BForm.Control
          name="description"
          as="textarea"
          onChange={handleChange}
          value={tender.description}
          rows="3"
          disabled={props.isDisabled}
          required
        />
      </BForm.Group>
      <Button variant="primary" type="submit" disabled={props.isDisabled}>
        {props.edit ? "Edit" : "Create"}
      </Button>
    </BForm>
  );
}

import React from "react";
import { Navbar as BNavbar, Nav } from "react-bootstrap";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faStar } from "@fortawesome/free-solid-svg-icons";

export default function Navbar() {
  return (
    <BNavbar bg="dark" variant="dark" expand="lg" className="heading">
      <BNavbar.Brand as={Link} to="/">
        <FontAwesomeIcon icon={faStar} /> Tender App
      </BNavbar.Brand>
      <BNavbar.Toggle aria-controls="basic-navbar-nav" />
      <BNavbar.Collapse id="basic-navbar-nav">
        <Nav className="mr-auto">
          <Nav.Link as={Link} to="/create">Create</Nav.Link>
        </Nav>
      </BNavbar.Collapse>
    </BNavbar>
  );
}

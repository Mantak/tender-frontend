import React from "react";

export default function Panel(props) {
  let className = 'panel';
  props.className && (className += ' ' + props.className);

  return <div className={className}>{props.children}</div>;
}

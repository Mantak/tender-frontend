import React from "react";
import { Container } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function Footer() {
  const year = new Date().getFullYear();
  return (
    <Container className="footer text-white text-right mt-4">
      <Link to="/">Tender App</Link> © {year}
    </Container>
  );
}

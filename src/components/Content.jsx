import React from "react";

export default function Content(props) {
  let className = "content";
  props.className && (className += " " + props.className);

  return <div className={className}>{props.children}</div>;
}

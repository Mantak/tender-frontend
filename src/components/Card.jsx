import React from "react";
import { Card as BCard } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash, faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

export default function Card(props) {
  return (
    <BCard className="tender-card">
      <BCard.Body>
        <BCard.Title>{props.title}</BCard.Title>
        <BCard.Text>{props.description}</BCard.Text>
        <BCard.Link as={Link} to={`/edit/${props.id}`} className="edit-btn">
          <FontAwesomeIcon icon={faPencilAlt} />
        </BCard.Link>
        <BCard.Link
          className="delete-btn"
          onClick={(event) => {
            event.preventDefault();
            props.onDelete(props.id);
          }}
          href="#"
        >
          <FontAwesomeIcon icon={faTrash} />
        </BCard.Link>
      </BCard.Body>
    </BCard>
  );
}

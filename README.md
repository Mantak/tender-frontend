### DealLink full stack developer assignment Frontend
Tender CRUD. It is responsible doing front-end stuff. It contacts with Tender Back-end via API calls.

### What I used to run this application?

1. NodeJS 13.10.x

1. NPM 6.14.x

### How to run this application?

1. Clone this repo

1. `cd tender-frontend && npm install`

1. Make sure your Tender Backend is running as well.

1. Go to src/config.js, check your API URL

1. npm start`

1. It will run on your browser automatically after this
